# iwantmyenvironment

**Warning:** This repository is currently a *wasteland*, and while I *do* expect to do something here later, it's apparently not happening now. So, install and use at your own risk!

This project serves as an easy way for me to download, install, and configure all of the dotfiles listed in [this group](https://gitlab.com/myriacore-dotfiles). It should allow you to go from a clean install of [manjaro i3](https://www.manjaro.org/download/community/i3/) to a fully customized setup with my dotfiles installed. 

